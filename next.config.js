/** @type {import('next').NextConfig} */
require("dotenv").config();

const nextConfig = {
  reactStrictMode: true,
  env: {
    host: 'http://localhost:9000'
  }
}

module.exports = nextConfig
