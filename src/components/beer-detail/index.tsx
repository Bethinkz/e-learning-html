
import { Card } from 'antd';
import { Typography } from 'antd';
import styles from './index.module.sass'

const { Text, Link } = Typography;

const BeerDetail = (props: any) => {
    return <Card className='' title={props.beerNumber + ' : ' + props.beerState.name}>
        {props.beerState && <div key={props.beerState._id}>
            <div className={styles.containerCard}>
                <p>Brand: <Text strong>{props.beerState.brand}</Text></p>
                <p>Name: <Text strong>{props.beerState.name}</Text></p>
                <p>Style: <Text strong>{props.beerState.style}</Text></p>
                <p>Hop: <Text strong>{props.beerState.hop}</Text></p>
                <p>Yeast: <Text strong>{props.beerState.yeast}</Text></p>
                <p>Malts: <Text strong>{props.beerState.malts}</Text></p>
                <p>IBU: <Text strong>{props.beerState.ibu}</Text></p>
                <p>Alcohol: <Text strong>{props.beerState.alcohol}</Text></p>
                <p>BLG: <Text strong>{props.beerState.blg}</Text></p>
                <p>RandomCount: <Text strong>{props.beerState.randomCount}</Text></p>
            </div>
        </div>}
    </Card>
}

export default BeerDetail