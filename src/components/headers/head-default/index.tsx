import Head from 'next/head'

const HeadDefault = () => {
    return <Head>
        <title>Beer bizcuit example</title>
        <meta name="description" content="Generated for Beer bizcuit example" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
    </Head>
}

export default HeadDefault