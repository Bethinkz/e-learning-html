import React from "react";
import { Layout, Space } from "antd";
import { Switch } from 'antd';
import { BulbOutlined } from '@ant-design/icons';

import stylesDarkmode from './index-darkmode.module.sass';
import stylesLightmode from './index.module.sass';

interface LayoutProps {
  children: React.ReactNode;
}

const { Header, Footer, Sider, Content } = Layout;

const Dashboard: React.FunctionComponent<LayoutProps> = ({ children }) => {
  let styles = stylesLightmode;
  const [isDarkMode, setIsDarkMode] = React.useState(false);
  if (isDarkMode) {
    styles = stylesDarkmode
  }

  const onChange = (checked: boolean) => {
    setIsDarkMode(checked);
  };

  return (
    <Space direction="vertical" style={{ width: '100%' }} size={[0, 48]}>
      <Layout id="dashboard-layout">
        <Header className={styles.antHeader}>
          <div style={{ width: 400 }}>Oh my Beer.</div>
          <div className={styles.toggleBox} style={{ paddingRight: 16 }}>
            <Switch checkedChildren="Dark mode" unCheckedChildren="Light mode" onChange={onChange} />
          </div>
        </Header>
        <Content className={styles.antLayout}>{children}</Content>
        <Footer className={styles.antFooter}>Used for applying for jobs at Biscuits Company only.</Footer>
      </Layout>
    </Space>
  );
};

export default Dashboard;
