
import { Card, Skeleton } from 'antd';

const Loading = () => {
    return <Card className=''>
        <Skeleton active />
    </Card>
}

export default Loading;