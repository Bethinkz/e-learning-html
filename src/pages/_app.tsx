import '@/styles/globals.sass'
import type { AppProps } from 'next/app'

import Header from 'src/components/headers/head-default/index';
import Layout from 'src/components/layouts/dashboard/index';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Header></Header>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>)
}
