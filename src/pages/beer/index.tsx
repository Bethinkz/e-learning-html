import { useState } from 'react';
import { Col, Row } from 'antd';
import { Button } from 'antd';
import { message } from 'antd';
import { StepBackwardOutlined, StepForwardOutlined, PlusOutlined } from '@ant-design/icons';

//component
import { LottieDefault, LottieWothoutLoop } from 'src/components/lottis-player';
import BeerDetail from 'src/components/beer-detail';
import Loading from 'src/components/loading';

//service
import * as BeerService from 'src/services/beer.service';

//interface
import { Beer } from 'src/models/beer';

import styles from './index.module.sass';

const BeerComponent = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const [loadingState, setloadingState] = useState<boolean>(false);
  const [curentBeerState, setCurentBeerState] = useState<number>(0);
  const [beerListState, setBeerListState] = useState<Beer[]>([]);

  // set timeout for more loading time
  async function getData() {
    setloadingState(true);
    setTimeout(async () => {
      const result = await BeerService.getRandomBeer();
      setBeerListState([...beerListState, result])
      setloadingState(false);
    }, 1500)
  }

  function addBeer() {
    messageApi.open({
      type: 'success',
      content: 'Tada!! this feature is in progress should be release soon!',
    });
  }

  function nextBeer() {
    if (beerListState.length <= curentBeerState) {
      getData()
      setCurentBeerState(curentBeerState + 1);
    } else {
      const beerIndex = curentBeerState + 1;
      setCurentBeerState(beerIndex);
    }
  }

  function previosBeer() {
    if (curentBeerState > 0) {
      const beerIndex = curentBeerState - 1;
      setCurentBeerState(beerIndex);
    }
  }

  return (
    <div>
      {contextHolder}
      {!loadingState && (!beerListState.length ? <h1 className={styles.headerName}>Click Next button to Generate beer!</h1> : <h1 className={styles.headerName}>This is your beer!</h1>)}
      {loadingState ? <h1 className={styles.headerName}>Generating.....</h1> : ''}

      <div className={styles.mainContainer}>
        <Row gutter={16}>
          <Col className="gutter-row" xs={24} sm={24} md={24} lg={12} xl={12}>
            <div className={styles.lottieContainer}>
              {(!loadingState && !beerListState.length) && <LottieDefault name={'99274-loading'} />}
              {loadingState && <LottieDefault name={'19713-six-pack-beer'} />}
              {(!loadingState && beerListState.length) && <LottieWothoutLoop name={beerListState[curentBeerState - 1].randomCount <= 5 ? '48952-a-simple-drunk-beer-bottle' : '1705-beer-animate'} />}
            </div>
          </Col>

          <Col className={'gutter-row ' + styles.cardList} xs={24} sm={24} md={24} lg={12} xl={12}>
            <Row gutter={16} className={styles.addBeerContainer}>
              <Col span={24}>
                <Button
                  className={styles.btnAddBeer}
                  icon={<PlusOutlined />}
                  loading={loadingState}
                  onClick={() => addBeer()}
                >
                  Add some beer?
                </Button>
              </Col>
            </Row>
            {beerListState[curentBeerState - 1] && <Row gutter={16}>
              <Col span={24}>
                {!loadingState && <BeerDetail beerState={beerListState[curentBeerState - 1]} beerNumber={curentBeerState}></BeerDetail>}
                {loadingState && <Loading />}
              </Col>
            </Row>}
          </Col>
          <Col className={'gutter-row ' + styles.btnAction} span={24}>
            {(beerListState[curentBeerState - 1] && curentBeerState > 1) && <Button
              className={styles.btnPrevious}
              icon={<StepBackwardOutlined />}
              loading={loadingState}
              onClick={() => previosBeer()}
            >
              Previous
            </Button>}
            <Button
              className={styles.btnNext}
              icon={<StepForwardOutlined />}
              loading={loadingState}
              onClick={() => nextBeer()}
            >
              Next
            </Button>
          </Col>
        </Row>
      </div>
    </div>
  )
};
export default BeerComponent;
