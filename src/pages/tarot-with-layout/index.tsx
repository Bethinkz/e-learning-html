import { useState } from 'react';
import { LottieDefault, LottieWothoutLoop } from 'src/components/lottis-player';
import { Input } from 'antd';
import { Image } from 'antd';
import { Button, Space } from 'antd';
import { Row, Col } from 'antd';

import tarotJson from 'src/seeding-database/tarot.json'

//*interface
import Styles from './index.module.sass'

interface Tarot {
    name: string;
    url: string;
    description_card: string;
    meaning_card: string;
    isReverse: boolean;
    isDelete: boolean;
}

const tarotInit: Tarot = {
    name: "",
    url: "https://sv1.picz.in.th/images/2023/01/27/LBNbCu.png",
    description_card: "",
    meaning_card: "",
    isReverse: true,
    isDelete: false
}

const TaroComponent = () => {
    const [tarotListState, setTarotListState] = useState<Tarot[]>(tarotJson);
    const [currentTarotState, setCurrentTarotState] = useState<Tarot>(tarotInit);

    const randomText = (): [string, number] => {
        const rndNumber = Math.floor(Math.random() * 43) + 0;
        return [rndNumber.toString(), rndNumber];
    }

    return (<div>
        <Row gutter={16}>
            <Col className="gutter-row" xs={0} sm={4} md={5} lg={6} xl={6}></Col>
            <Col className="gutter-row" xs={24} sm={16} md={14} lg={12} xl={12}>
                <Row gutter={[16, 16]}>
                    <Col className="gutter-row" span={24}>
                        <div className={Styles.setCenter}>
                            <Image
                                height={400}
                                src={currentTarotState.url}
                            />
                        </div>
                    </Col>
                    <Col className="gutter-row" span={24}>
                        <h1 className={Styles.header}>Type Your Name</h1>
                    </Col>
                    <Col className="gutter-row" span={24}>
                        <Input className={Styles.Inputhome} placeholder="Type your name" />
                    </Col>
                    <Col className="gutter-row" span={24}>
                        <Input className={Styles.Inputhome} placeholder="Type your name" />
                    </Col>
                    <Col className="gutter-row" span={24}>
                        <div className={Styles.setCenter}>
                            <Button className={Styles.btntarot} onClick={() => {
                                const [randTxt, randNumber] = randomText();
                                setCurrentTarotState(tarotListState[randNumber]);
                            }}>Turn Your Fortune</Button>
                        </div>
                    </Col>
                    <Col className="gutter-row" span={24}>
                        {currentTarotState.description_card}
                    </Col>
                    <Col className="gutter-row" span={24}>
                        {currentTarotState.meaning_card}
                    </Col>
                </Row>
            </Col>
            <Col className="gutter-row" xs={0} sm={4} md={5} lg={6} xl={6}></Col>
        </Row>
    </div >)
}
export default TaroComponent;
