import { useState } from 'react';
import { LottieDefault, LottieWothoutLoop } from 'src/components/lottis-player';
import { Input } from 'antd';
import { Image } from 'antd';
import { Button, Space } from 'antd';
import { message } from 'antd';

import tarotJson from 'src/seeding-database/tarot.json'

//*interface
import Styles from './index.module.sass'

interface Tarot {
    name: string;
    url: string;
    description_card: string;
    meaning_card: string;
    isReverse: boolean;
    isDelete: boolean;
}

const tarotInit: Tarot = {
    name: "",
    url: "https://sv1.picz.in.th/images/2023/01/27/LBNbCu.png",
    description_card: "",
    meaning_card: "",
    isReverse: true,
    isDelete: false
}

const TaroComponent = () => {
    const [tarotListState, setTarotListState] = useState<Tarot[]>(tarotJson);
    const [currentTarotState, setCurrentTarotState] = useState<Tarot>(tarotInit);

    const randomText = (): [string, number] => {
        const rndNumber = Math.floor(Math.random() * 43) + 0;
        return [rndNumber.toString(), rndNumber];
    }

    return (<div>
        <Image
            height={200}
            src={currentTarotState.url}
        />
        <h1 className={Styles.header}> Type Your Name</h1>
        <Input className={Styles.Inputhome} placeholder="Type your name" />
        <br></br>
        <Button className={Styles.btntarot} onClick={() => {
            const [randTxt, randNumber] = randomText();
            setCurrentTarotState(tarotListState[randNumber]);
        }}>Turn Your Fortune</Button>
        { currentTarotState.description_card }
        { currentTarotState.meaning_card }

    </div>)


}
export default TaroComponent;
