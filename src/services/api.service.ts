
import axios from 'axios'

export const getRandomBeer = async () => {
    const endpoint = process.env.host
    const url = 'https://random-data-api.com/api/beer/random_beer'
    const data = await axios.get(endpoint + url)
        .catch((error) => {
            return error.response;
        });
    return data;
}