
import axios from 'axios'

import { Beer } from 'src/models/beer';

export const getRandomBeer = async (): Promise<Beer> => {
    const endpoint = process.env.host
    const url = '/api/beer/random_beer'
    console.log(endpoint)
    console.log(url)
    const resp = await axios.get(endpoint + url)
        .catch((error) => {
            return error.response;
        });
    console.log(resp.data);
    return resp.data;
}

export const getBeerList = async (): Promise<Beer[]> => {
    const endpoint = process.env.host
    const url = '/api/beer/random_beer'
    const data = await axios.get(endpoint + url)
        .catch((error) => {
            return error.response;
        });
    return data;
}

export const addBeer = async (beerObject: Beer): Promise<Beer> => {
    const endpoint = process.env.host
    const url = '/api/beer/create_beer'
    const body = beerObject;
    const data = await axios.post(endpoint + url, body)
        .catch((error) => {
            return error.response;
        });
    return data;
}