import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyBIjZqIeHgJsYD2ghkraXU8c1TdHCcn9Ko",
    authDomain: "agent-nurse.firebaseapp.com",
    databaseURL: "https://agent-nurse-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "agent-nurse",
    storageBucket: "agent-nurse.appspot.com",
    messagingSenderId: "792703221809",
    appId: "1:792703221809:web:579c2e92290419bc0f1510"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const database = getFirestore(app);